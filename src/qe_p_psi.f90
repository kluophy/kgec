!
! Copyright (C) 2017- Orbital-free DFT group at University of Florida
!
! This program is free software; you can redistribute it and/or modify 
! it under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2 of the License, or 
! any later version. See the file LICENSE in the root directory of the 
! present distribution, or http://www.gnu.org/copyleft/gpl.txt ,
! or contact developers via e-mail: calderin@lcalderin.net, or paper mail:
!
! Quantum Theory Project
! University of Florida
! P.O. Box 118435
! Gainesville, FL 32611
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! Authors: Lazaro Calderin and Valentin Karasiev
!-----------------------------------------------------------------------
! 


module qe_p_psi
!
! Get the <p_i|psi> or <beta_i|psi> from Quantum Espresso
! Get the mapping of the projectors index
!
use kinds,                ONLY : dp
use ions_base,            ONLY : nat, ityp, ntyp => nsp
use cell_base,            ONLY : at, tpiba2
use constants,            ONLY : rytoev
use gvect,                ONLY : g, ngm
use lsda_mod,             ONLY : nspin
#if defined(__QE6) || defined(__QE62) || defined(__QE64)
use klist,                ONLY : xk, nks, nkstot, ngk, igk_k
#elif defined(__QE512) || defined(__QE54)
use klist,                ONLY : xk, nks, nkstot
#endif
use io_files,             ONLY : iunpun, nwordwfc, iunwfc
#if defined(__QE6) || defined(__QE62) || defined(__QE64)
use wvfct,                ONLY : nbnd, et, npw, npwx, g2kin
use gvecw,                ONLY : ecutwfc
#elif defined(__QE54)
use wvfct,                ONLY : nbnd, et, igk, npw, npwx, g2kin
use gvecw,                ONLY : ecutwfc
#elif defined(__QE512)
use wvfct,                ONLY : nbnd, et, ecutwfc, igk, npw, npwx, g2kin
#endif
use uspp,                 ONLY : nkb, vkb !, qq
use uspp_param,           ONLY : upf, nh, nhm, nbetam
use noncollin_module,     ONLY : noncolin, npol
#if defined(__QE64)
use wavefunctions,        ONLY : evc
#else
use wavefunctions_module, ONLY : evc
#endif
use io_global,            ONLY : ionode, ionode_id, stdout
use becmod,               ONLY : calbec, bec_type, allocate_bec_type, deallocate_bec_type

!   use mp,                   ONLY : mp_bcast
!   use mp_world,             ONLY : world_comm

implicit none
!   integer, allocatable, public :: p_index(:,:) ! QE projector index mapping back to atom and lm

   type(bec_type), public, allocatable :: becp(:)  ! <p|\psi>
   public                 :: p_psi ! Subroutine

private
contains

   subroutine p_psi()
   implicit none

      integer :: ik

      allocate( becp(1:nks) )

      do ik = 1, nks
         call allocate_bec_type(nkb, nbnd, becp(ik))
         !
         ! get the k+G vectors for the ik k-point
         !
#if defined(__QE512) || defined(__QE54)
         call gk_sort (xk (1, ik), ngm, g, ecutwfc / tpiba2, npw, igk, g2kin)
#else
         npw = ngk(ik)
#endif

         !
         !   read eigenfunctions
         !
         call davcio (evc, 2*nwordwfc, iunwfc, ik, - 1)

         !
         ! calculate becp = <psi|beta>
         !

#if defined(__QE512) || defined(__QE54)
         call init_us_2 (npw, igk, xk (1, ik), vkb)
#else
         call init_us_2 (npw, igk_k(1,ik), xk(1,ik), vkb)
#endif
         call calbec ( npw, vkb, evc, becp(ik) )

!        IF (noncolin) &
!           CALL compute_sigma_avg(sigma_avg(1,1,ik),becp%nc,ik,lsigma)

      enddo

   !
   ! Mapping of indexes l and m for each type of atom nt -> nh(nt) in the following way:
   !
   !do it = 1, ntyp
   !   ih = 1
   !   do ib = 1, upf(it)%nbeta
   !      do im = 1, 2 * upf(it)%lll (ib) + 1
   !         ih = ih + 1
   !      enddo
   !enddo
   !enddo

   !
   ! Mapping of all atoms projectors index nh index into a jkb index continously and
   ! ordered by type of atom.
   ! Recalculating that here and storing it
   !

   !jkb = 7*nbetam
   !allocate( p_index( 1:nat, 1:jkb ) )

   !jkb = 0
   !do it=1, ntyp
   !   do ia=1, nat
   !      if ( ityp(ia) .eq. it ) then
   !         do ih = 1, nh (it)
   !            jkb = jkb + 1
   !            p_index(ia,ih) = jkb  ! Projectors
   !         enddo
   !     endif
   !  enddo
   !enddo

   end subroutine p_psi

end module qe_p_psi
