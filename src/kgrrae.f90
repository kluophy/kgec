module kgrrae
!
! Calculation of refraction, reflection and absoption coefficients,
! and EELS.
!
use kinds,      only : DP
use qevars,     only : pi, C_AU
use kgecvars,   only : nw, wgrid
use kgepsilon1, only : epsilon1_atrace
use kgepsilon2, only : epsilon2_atrace

implicit none

   private

   real(DP), allocatable, public :: reflection(:), absorption(:), &
                                    n_real(:), n_imag(:), eels(:)

   real(DP), allocatable :: epsilon_module(:)

   public :: rrae

contains

subroutine rrae 

    allocate( reflection(0:nw) )
    allocate( absorption(0:nw) )
    allocate( n_real(0:nw) )
    allocate( n_imag(0:nw) )
    allocate( epsilon_module(0:nw) )
    allocate( eels(0:nw) )    

    epsilon_module = sqrt(   epsilon1_atrace * epsilon1_atrace &
                           + epsilon2_atrace * epsilon2_atrace )

    ! Refraction index ( real part )
    n_real = sqrt( 0.5_dp * ( epsilon_module + epsilon1_atrace) )

    ! Refraction index ( imaginary part ) 
    n_imag = sqrt( 0.5_dp * ( epsilon_module - epsilon1_atrace  ) )

   ! Reflection
   reflection = &
         ( ( 1.0_dp - n_real ) * ( 1.0_dp - n_real ) + n_imag * n_imag ) &
       / ( ( 1.0_dp + n_real ) * ( 1.0_dp + n_real ) + n_imag * n_imag )

    ! Absorpttion
    !absorption = 4.0_dp * pi * sigma1_atrace / n_real / C_AU
    absorption = wgrid * epsilon2_atrace / n_real / C_AU

    ! Electron Energy Loss Spectrum
    eels = epsilon2_atrace / (epsilon_module * epsilon_module )

end subroutine rrae

end module kgrrae
