!
! Copyright (C) 2017- Orbital-free DFT group at University of Florida
!
! This program is free software; you can redistribute it and/or modify 
! it under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2 of the License, or 
! any later version. See the file LICENSE in the root directory of the 
! present distribution, or http://www.gnu.org/copyleft/gpl.txt ,
! or contact developers via e-mail: calderin@lcalderin.net, or paper mail:
!
! Quantum Theory Project
! University of Florida
! P.O. Box 118435
! Gainesville, FL 32611
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! Authors: Lazaro Calderin and Valentin Karasiev
!-----------------------------------------------------------------------
! 


module kgecout
use qevars
use kgecvars
use kgsigma1
use kgsigma2
use kgepsilon1
use kgepsilon2
use kgrrae

implicit none


private

   integer :: iw                          ! Frequency index

   integer :: ik                          ! counter on k-points
                                          ! always local (parallelization is hidden) 
                                         

   integer :: ib1,  ib2                    ! global counters on bands
   integer :: ibb1                         ! local counter on bands


   integer :: ix1, ix2                     ! labels x, y an z

   character(256) :: filename

public :: kgec_output_sigma, kgec_output_epsilon, kgec_output_rrae

contains

   subroutine kgec_output_sigma

   real(DP) :: sigma_aux(3,3,0:nw),       sigma_aux_atrace(0:nw)
   real(DP) :: sigma_aux_intra(3,3,0:nw), sigma_aux_atrace_intra(0:nw)
   real(DP) :: sigma_aux_degen(3,3,0:nw), sigma_aux_atrace_degen(0:nw)
   real(DP) :: sigma_aux_inter(3,3,0:nw), sigma_aux_atrace_inter(0:nw)

   real(DP) :: sigma_dc_aux(3,3),       sigma_dc_aux_atrace
   real(DP) :: sigma_dc_aux_intra(3,3), sigma_dc_aux_atrace_intra
   real(DP) :: sigma_dc_aux_degen(3,3), sigma_dc_aux_atrace_degen
   real(DP) :: sigma_dc_aux_inter(3,3), sigma_dc_aux_atrace_inter

   ! Frequency grid in eV
   wgrid = wgrid * AUTOEV
   
   ! SIGMA1 OUTPUT

   write(stdout,*) ' '
   write(stdout,*)           '    ----------------------------------------------------------------'
   write(stdout,*)           "                 SIGMA1 RELATED OUTPUT (ohm meter)^-1"
   write(stdout,*)           '    ----------------------------------------------------------------'


   WRITESIGMA1: select case ( calculation )
   case( 'tensor' )

      ! Sigma1

      select case( ac )
      case(.true.)

         sigma_aux          = AU_TO_OHMM_INV * sigma1        ! sigma1 in 1/a.u. to 1/(ohm meter)
         sigma_aux_atrace   = AU_TO_OHMM_INV * sigma1_atrace ! sigma1 in 1/a.u. to 1/(ohm meter)

         if ( sigma1_notintra ) then
            write(filename,"('kgec-sigma1-tensor-', A2 ,'inter-only.dat')" ) deltarep
         else
            write(filename,"('kgec-sigma1-tensor-', A2 ,'.dat')" ) deltarep
         endif

         open( 11,file=filename, action='write')
         write(11,'(A)')    '# Electric conductivity tensor in the xyz system of reference'
         write(11,'(A,L4,2x,A,2x,A2)') '#   sigma1_exact=', sigma1_exact, 'deltarep=', deltarep      
         write(11,'(A,f10.6,A)'     ) '#      deltawidth=', deltawidth, 'eV'
         write(11,'(A)'             ) '#      hbar*w (eV)                           sigma1(w) ( 1/(ohm-meter) )'
         write(11,'(A)',advance='no') '#                               11                  12                  13'
         write(11,'(A)',advance='no') '                  21                  22                  23                  31'
         write(11,'(A)') '                  32                  33               trace/3'

         do iw = iaux, nw
            write(11,'(1p,11es20.7e3)') wgrid(iw), (((sigma_aux(ix1,ix2,iw)), ix2=1,3),ix1=1,3), &
                                                   sigma_aux_atrace(iw)
         enddo
         close(11)

      end select ! ac

      select case(dc)
      case(.true.)

         sigma_dc_aux        = AU_TO_OHMM_INV * sigma1_dc
         sigma_dc_aux_atrace = AU_TO_OHMM_INV * sigma1_dc_atrace

         if ( sigma1_notintra ) then
            write(stdout,*) "    sigma1 DC tensor: inter-band only contributions"
         else    
            write(stdout,*) "    sigma1 DC tensor: all contributions"
         endif 

         do ix1=1,3
            write(stdout,'(1p,3es20.7e3)')  ( sigma_dc_aux(ix1,ix2), ix2=1,3 )
         enddo
         
         if (sigma1_notintra ) then
            write(stdout,*) "      DC ave. trace (inter-band only)          :", sigma_dc_aux_atrace
         else
            write(stdout,*) "      DC ave. trace (all contributions)        :", sigma_dc_aux_atrace
         endif

      end select
         

      select case ( decomposition ) ! sigma1 tensor decomp
      case(.true.)

         ! Sigma1

         select case( ac )
         case(.true.)
         
         sigma_aux_intra   = AU_TO_OHMM_INV * sigma1_intra     ! sigma1 in 1/a.u. to 1/(ohm meter)
         sigma_aux_degen   = AU_TO_OHMM_INV * sigma1_degen     ! sigma1 in 1/a.u. to 1/(ohm meter)
         sigma_aux_inter   = AU_TO_OHMM_INV * sigma1_inter     ! sigma1 in 1/a.u. to 1/(ohm meter)

         sigma_aux_atrace_intra   = AU_TO_OHMM_INV * sigma1_atrace_intra     ! sigma1 in 1/a.u. to 1/(ohm meter)
         sigma_aux_atrace_degen   = AU_TO_OHMM_INV * sigma1_atrace_degen     ! sigma1 in 1/a.u. to 1/(ohm meter)
         sigma_aux_atrace_inter   = AU_TO_OHMM_INV * sigma1_atrace_inter     ! sigma1 in 1/a.u. to 1/(ohm meter)

         
         write(filename,"('kgec-sigma1-tensor-degen-', A2 ,'.dat')" ) deltarep
         open(11,file=filename, action='write')
         write(11,'(A)') '# Electric conductivity tensor in the xyz system of reference'
         write(11,'(A,f10.6,A)'     ) '#      deltawidth=', deltawidth, 'eV'
         write(11,'(A)'             ) '#      hbar*w (eV)                           sigma1(w) ( 1/(ohm-meter) )'
         write(11,'(A)',advance='no') '#                               11                  12                  13'
         write(11,'(A)',advance='no') '                  21                  22                  23                  31'
         write(11,'(A)') '                  32                  33               trace/3'
         do iw = iaux, nw
            write(11,'(1p,11es20.7e3)') wgrid(iw), (((sigma_aux_degen(ix1,ix2,iw)), ix2=1,3),ix1=1,3), &
                                                   sigma_aux_atrace_degen(iw)
         enddo
         close(11)


         write(filename,"('kgec-sigma1-tensor-inter-', A2 ,'.dat')" ) deltarep
         open(11,file=filename, action='write')
         write(11,'(A)') '# Electric conductivity tensor in the xyz system of reference'
         write(11,'(A,f10.6,A)'     ) '#      deltawidth=', deltawidth, 'eV'
         write(11,'(A)'             ) '#      hbar*w (eV)                           sigma1(w) ( 1/(ohm-meter) )'
         write(11,'(A)',advance='no') '#                               11                  12                  13'
         write(11,'(A)',advance='no') '                  21                  22                  23                  31'
         write(11,'(A)') '                  32                  33               trace/3'
         do iw = iaux, nw
            write(11,'(1p,11es20.7e3)') wgrid(iw), (((sigma_aux_inter(ix1,ix2,iw)), ix2=1,3),ix1=1,3), &
                                                   sigma_aux_atrace_inter(iw)
         enddo
         close(11)

         end select ! ac

         select case( dc )
         case(.true.)

            sigma_dc_aux_intra        = AU_TO_OHMM_INV * sigma1_dc_intra
            sigma_dc_aux_atrace_intra = AU_TO_OHMM_INV * sigma1_dc_atrace_intra
            sigma_dc_aux_degen        = AU_TO_OHMM_INV * sigma1_dc_degen
            sigma_dc_aux_inter        = AU_TO_OHMM_INV * sigma1_dc_inter
            sigma_dc_aux_atrace_degen = AU_TO_OHMM_INV * sigma1_dc_atrace_degen
            sigma_dc_aux_atrace_inter = AU_TO_OHMM_INV * sigma1_dc_atrace_inter


            write(stdout,*) "    sigma1 DC tensor: intra-bands contribution"
            do ix1=1,3
               write(stdout,'(1p,3es20.7e3)')  ( sigma_dc_aux_intra(ix1,ix2), ix2=1,3 )
            enddo
            write(stdout,*) "      DC ave. trace (intraband contribution)   :", sigma_dc_aux_atrace_intra

            write(stdout,*) "    sigma1 DC tensor: degeneracies contribution"
            do ix1=1,3
               write(stdout,'(1p,3es20.7e3)')  ( sigma_dc_aux_degen(ix1,ix2), ix2=1,3 )
            enddo
            write(stdout,*) "      DC ave. trace (degeneracies contribution):", sigma_dc_aux_atrace_degen
 
            write(stdout,*) "    sigma1 DC tensor: inter-bands contribution"
            do ix1=1,3
               write(stdout,'(1p,3es20.7e3)')  ( sigma_dc_aux_inter(ix1,ix2), ix2=1,3 )
            enddo
            write(stdout,*) "      DC ave. trace (inter-bands contribution) :", sigma_dc_aux_atrace_inter

         end select
         
      end select ! sigma1 tensor decomp

   
   case( 'atrace' ) ! sigma1


      select case( ac )
      case(.true.)

         sigma_aux_atrace   = AU_TO_OHMM_INV * sigma1_atrace   ! sigma1 in 1/a.u. to 1/(ohm meter)

         if ( sigma1_notintra ) then
            write(filename,"('kgec-sigma1-atrace-', A2 ,'inter-only.dat')" ) deltarep
         else
            write(filename,"('kgec-sigma1-atrace-', A2 ,'.dat')" ) deltarep
         endif

         open(11,file=filename, action='write')
         write(11,'(A)') '# Electric conductivity tensor in the xyz system of reference'
         write(11,'(A,L4,2x,A,2x,A2)') '#   sigma1_exact=', sigma1_exact, 'deltarep=', deltarep      
         write(11,'(A)'              ) '#      hbar*w (eV)                           tr(sigma1(w))/3 ( 1/(ohm-meter) )'
         write(11,'(A)'              ) '#'

         do iw = iaux, nw
            write(11,'(1p,2es20.7e3)') wgrid(iw), sigma_aux_atrace(iw)
         enddo
         close(11)

      end select

      select case(dc)
      case(.true.)

         sigma_dc_aux_atrace = AU_TO_OHMM_INV * sigma1_dc_atrace
         
         if (sigma1_notintra ) then
            write(stdout,*) "      DC ave. trace (inter-band only)          :", sigma_dc_aux_atrace
         else
            write(stdout,*) "      DC ave. trace (all contributions)        :", sigma_dc_aux_atrace
         endif

      end select

      select case ( decomposition ) ! sigma1 decomp
      case( .true. )

         select case( ac )
         case(.true.)
         
         sigma_aux_atrace_intra = AU_TO_OHMM_INV &
                               * sigma1_atrace_intra ! 1/a.u. to 1/(ohm meter)
         sigma_aux_atrace_degen = AU_TO_OHMM_INV &
                               * sigma1_atrace_degen ! 1/a.u. to 1/(ohm meter)
         sigma_aux_atrace_inter = AU_TO_OHMM_INV &
                               * sigma1_atrace_inter ! 1/a.u. to 1/(ohm meter)

         write(filename,"('kgec-sigma1-atrace-decomp-', A2 ,'.dat')" ) deltarep
         open(11,file=filename, action='write')
         write(11,'(A)') &
         '# Electric conductivity tensor in the xyz system of reference'
         write(11,'(A,f10.6,A)'     ) '#      deltawidth=', deltawidth, 'eV'

         if ( .not. sigma1_notintra ) then
         
         write(11,'(A)'             ) &
         '#      hbar*w (eV)     tr(sigma1_intra)/3    tr(sigma1_degen)/3    &
                                &tr(sigma1_inter)/3    tr(sigma1)/3 ( 1/(ohm-meter) )'
         write(11,'(A)'             ) '#'
 
         do iw = iaux, nw
            write(11,'(1p,6es20.7e3)') wgrid(iw), sigma_aux_atrace_intra(iw),   &
                  sigma_aux_atrace_degen(iw), sigma_aux_atrace_inter(iw), sigma_aux_atrace(iw)
         enddo
         close(11)

         else
         
         write(11,'(A)'             ) &
         '#      hbar*w (eV)     tr(sigma1_degen)/3    tr(sigma1_inter)/3    &
                                               &tr(sigma1)/3 ( 1/(ohm-meter) )'
         write(11,'(A)'             ) '#'
 
         do iw = iaux, nw
            write(11,'(1p,5es20.7e3)') wgrid(iw), sigma_aux_atrace_degen(iw), sigma_aux_atrace_inter(iw), sigma_aux_atrace(iw)
         enddo
         close(11)
         
         endif

         end select ! ac

         select case( dc )
         case(.true.)

         sigma_dc_aux_atrace_intra = AU_TO_OHMM_INV * sigma1_dc_atrace_intra
         sigma_dc_aux_atrace_degen = AU_TO_OHMM_INV * sigma1_dc_atrace_degen
         sigma_dc_aux_atrace_inter = AU_TO_OHMM_INV * sigma1_dc_atrace_inter

         write(stdout,*) "      DC ave. trace (intra-bands contribution) :", sigma_dc_aux_atrace_intra
         write(stdout,*) "      DC ave. trace (degeneracies contribution):", sigma_dc_aux_atrace_degen
         write(stdout,*) "      DC ave. trace (inter-bands contribution) :", sigma_dc_aux_atrace_inter

         end select ! sigma1 decomp

      end select

      select case(ac)
      case(.true.)
         write(stdout,*) ""
         write(stdout,*) "    Real part of the conductivity tensor and/or average trace (in"
         write(stdout,*) "    1/(ohm-meter)) vs frequencies (in eV) saved to kgec-sigma1*.dat file(s)."
      end select

   end select WRITESIGMA1

   !  END OF SIGMA1 OUTPUT 

   !
   ! SIGMA2 OUTPUT
   !

   if (sigma2_via_eps1mod ) calculate_sigma2=.true. ! Output is independent of how sigma2 was calculated

   WRITESIGMA2: select case ( calculate_sigma2 )
   case( .true. )

   write(stdout,*) ' '
   write(stdout,*)           '    ----------------------------------------------------------------'
   write(stdout,*)           "                 SIGMA2 RELATED OUTPUT (ohm meter)^-1"
   write(stdout,*)           '    ----------------------------------------------------------------'

   select case ( dc )
   case( .true. )
        write(stdout,*) '    All sigma2 DC tensor components are zero.'
   end select

   select case ( calculation )
   case( 'tensor' )

      ! Sigma2

      select case( ac )
      case(.true.)

      sigma_aux        = AU_TO_OHMM_INV * sigma2 ! 1/a.u. to 1/(ohm meter)
      sigma_aux_atrace = AU_TO_OHMM_INV * sigma2_atrace

      if ( sigma2_notintra ) then
         write(filename,"('kgec-sigma2-tensor-inter-only.dat')" )
      else
         write(filename,"('kgec-sigma2-tensor.dat')" )
      endif

      open(11,file=filename, action='write')
      write(11,'(A)') '# Imaginary part of the electric conductivity tensor in the xyz system of reference'
      write(11,'(A)'             ) '#      hbar*w (eV)                           sigma2(w) ( 1/(ohm-meter) )'
      write(11,'(A)',advance='no') '#                               11                  12                  13'
      write(11,'(A)',advance='no') '                  21                  22                  23                  31'
      write(11,'(A)') '                  32                  33               trace/3'

      do iw = iaux, nw
         write(11,'(1p,11es20.7e3)') wgrid(iw), (((sigma_aux(ix1,ix2,iw)), ix2=1,3),ix1=1,3), &
                                                sigma_aux_atrace(iw)
      enddo
      close(11)

         write(stdout,*) ""
         write(stdout,*) "    Real part of the conductivity tensor and/or average trace (in"
         write(stdout,*) "    1/(ohm-meter)) vs frequencies (in eV) saved to kgec-sigma2*.dat file(s)."

      end select ! ac

      select case ( decomposition )
      case(.true.)

         ! sigma2

         select case( ac )
         case(.true.)

         sigma_aux_degen = AU_TO_OHMM_INV * sigma2_degen ! 1/a.u. to 1/(ohm meter)
         sigma_aux_inter = AU_TO_OHMM_INV * sigma2_inter ! 1/a.u. to 1/(ohm meter)

         sigma_aux_atrace_degen = AU_TO_OHMM_INV * sigma2_atrace_degen ! 1/a.u. to 1/(ohm meter)
         sigma_aux_atrace_inter = AU_TO_OHMM_INV * sigma2_atrace_inter ! 1/a.u. to 1/(ohm meter)


         write(filename,"('kgec-sigma2-tensor-degen.dat')" )
         open(11,file=filename, action='write')
         write(11,'(A)') &
         '# Electric conductivity tensor in the xyz system of reference'
         write(11,'(A)'             ) &
         '#      hbar*w (eV)                           sigma2(w) ( 1/(ohm-meter) )'
         write(11,'(A)',advance='no') &
         '#                               11                  12                  13'
         write(11,'(A)',advance='no') &
         '                  21                  22                  23                  31'
         write(11,'(A)') '                  32                  33               trace/3'
         do iw = iaux, nw
            write(11,'(1p,11es20.7e3)') wgrid(iw), &
                                        (((sigma_aux_degen(ix1,ix2,iw)), ix2=1,3),ix1=1,3), &
                                        sigma_aux_atrace_degen(iw)
         enddo
         close(11)

         write(filename,"('kgec-sigma2-tensor-inter.dat')" )
         open(11,file=filename, action='write')
         write(11,'(A)') &
         '# Electric conductivity tensor in the xyz system of reference'
         write(11,'(A)'             ) &
         '#      hbar*w (eV)                           sigma2(w) ( 1/(ohm-meter) )'
         write(11,'(A)',advance='no') &
         '#                               11                  12                  13'
         write(11,'(A)',advance='no') &
         '                  21                  22                  23                  31'
         write(11,'(A)') '                  32                  33               trace/3'
         do iw = iaux, nw
            write(11,'(1p,11es20.7e3)') wgrid(iw), &
                                       (((sigma_aux_inter(ix1,ix2,iw)), ix2=1,3),ix1=1,3), &
                                       sigma_aux_atrace_inter(iw)
         enddo
         close(11)

         end select ! ac

      end select
   
   case( 'atrace' )

      select case( ac )
      case(.true.)

      sigma_aux_atrace    = AU_TO_OHMM_INV &
                           * sigma2_atrace    ! 1/a.u. to 1/(ohm meter)

      if ( sigma2_notintra ) then
         write(filename,"('kgec-sigma2-atrace-inter-only.dat')" )
      else
         write(filename,"('kgec-sigma2-atrace.dat')" )
      endif

      open(11,file=filename, action='write')
      write(11,'(A)') &
      '# Imaginary part of the average trace of the electric &
      &conductivity tensor in the xyz system of reference'
      write(11,'(A)'             ) &
      '#      hbar*w (eV)                           tr(sigma2(w))/3 ( 1/(ohm-meter) )'
      write(11,* ) '#'

      do iw = iaux, nw
         write(11,'(1p,2es20.7e3)') wgrid(iw), sigma_aux_atrace(iw)
      enddo
      close(11)

      end select ! ac

      select case ( decomposition )
      case( .true. )

         ! sigma2
         select case( ac )
         case(.true.)
         
         sigma_aux_atrace_degen = AU_TO_OHMM_INV &
                               * sigma2_atrace_degen ! 1/a.u. to 1/(ohm meter)
         sigma_aux_atrace_inter = AU_TO_OHMM_INV &
                               * sigma2_atrace_inter ! 1/a.u. to 1/(ohm meter)

         write(filename,"('kgec-sigma2-atrace-decomp.dat')" )
         open(11,file=filename, action='write')
         write(11,'(A)') &
         '# Imaginary part of the average of the trace of the electric &
         &conductivity tensor in the xyz system of reference'
         write(11,'(A)'             ) &
         '#      hbar*w (eV)     tr(sigma2_degen)/3    tr(sigma2_inter)/3    &
         &tr(sigma2)/3 ( 1/(ohm-meter) )'
         write(11,* ) '#'
         do iw = iaux, nw
            write(11,'(1p,5es20.7e3)') wgrid(iw), sigma_aux_atrace_degen(iw), &
                                       sigma_aux_atrace_inter(iw), sigma_aux_atrace(iw)
         enddo
         close(11)

         end select ! ac

      end select

   end select

   select case(ac)
   case(.true.)
      write(stdout,*) ""
      write(stdout,*) "    Real part of the conductivity tensor and/or average trace (in"
      write(stdout,*) "    1/(ohm-meter)) vs frequencies (in eV) saved to kgec-sigma2*.dat file(s)."
   end select

   end select WRITESIGMA2

   ! END OF SIGMA2 related output


    ! Restore Frequency grid to  Ha
    wgrid = wgrid / AUTOEV

   
   end subroutine kgec_output_sigma

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   subroutine kgec_output_epsilon

   ! Frequency grid in eV
   wgrid = wgrid * AUTOEV
   
   !
   ! EPSILON1 OUTPUT
   !

   WRITEEPSILON1: select case ( calculate_epsilon1 )
   case( .true. )

   write(stdout,*) ' '
   write(stdout,*)           '    ----------------------------------------------------------------'
   write(stdout,*)           "                      EPSILON1 RELATED OUTPUT "
   write(stdout,*)           '    ----------------------------------------------------------------'

   select case ( calculation )
   case( 'tensor' )

      select case( ac )
      case(.true.)

      if ( sigma2_notintra ) then
         write(filename,"('kegc-epsilon1-tensor-inter-only.dat')" )
      else
         write(filename,"('kegc-epsilon1-tensor.dat')" )
      endif

      open(11,file=filename, action='write')
      write(11,'(A)') '# Real part of the dielectric conductivity tensor in the xyz system of reference'
      write(11,'(A)'             ) '#      hbar*w (eV)                           epsilon1(w) (dimensionless)'
      write(11,'(A)',advance='no') '#                               11                  12                  13'
      write(11,'(A)',advance='no') '                  21                  22                  23                  31'
      write(11,'(A)') '                  32                  33               trace/3'

      do iw = iaux, nw
         write(11,'(1p,11es20.7e3)') wgrid(iw), (((epsilon1(ix1,ix2,iw)), ix2=1,3),ix1=1,3), &
                                                epsilon1_atrace(iw)
      enddo
      close(11)

      end select ! ac

      select case(dc)
      case(.true.)

         if ( sigma2_notintra ) then
            write(stdout,*) "    epsilon1 DC tensor: inter-band only contributions"
         else    
            write(stdout,*) "    epsilon1 DC tensor: all contributions"
         endif 

         do ix1=1,3
            write(stdout,'(1p,3es20.7e3)')  ( epsilon1_dc, ix2=1,3 )
         enddo
         
         if (sigma2_notintra ) then
            write(stdout,*) "      DC ave. trace (inter-band only)          :", epsilon1_dc_atrace
         else
            write(stdout,*) "      DC ave. trace (all contributions)        :", epsilon1_dc_atrace
         endif

      end select

      select case ( decomposition )
      case(.true.)

         select case( ac )
         case( .true. )

         write(filename,"('kgec-epsilon1-tensor-intra.dat')" )
         open(11,file=filename, action='write')
         write(11,'(A)') &
         '# Dielectric conductivity tensor in the xyz system of reference'
         write(11,'(A)'             ) &
         '#      hbar*w (eV)                           epsilon1(w) (dimensionless)'
         write(11,'(A)',advance='no') &
         '#                               11                  12                  13'
         write(11,'(A)',advance='no') &
         '                  21                  22                  23                  31'
         write(11,'(A)') '                  32                  33               trace/3'
         do iw = iaux, nw
            write(11,'(1p,11es20.7e3)') wgrid(iw), &
                                       (((epsilon1_intra(ix1,ix2,iw)), ix2=1,3),ix1=1,3), &
                                       epsilon1_atrace_intra(iw)
         enddo
         close(11)


         write(filename,"('kegec-epsilon1-tensor-degen.dat')" )
         open(11,file=filename, action='write')
         write(11,'(A)') &
         '# Dielectric tensor in the xyz system of reference'
         write(11,'(A)'             ) &
         '#      hbar*w (eV)                           epsilon1(w) (dimensionless)'
         write(11,'(A)',advance='no') &
         '#                               11                  12                  13'
         write(11,'(A)',advance='no') &
         '                  21                  22                  23                  31'
         write(11,'(A)') '                  32                  33               trace/3'
         do iw = iaux, nw
            write(11,'(1p,11es20.7e3)') wgrid(iw), &
                                        (((epsilon1_degen(ix1,ix2,iw)), ix2=1,3),ix1=1,3), &
                                        epsilon1_atrace_degen(iw)
         enddo
         close(11)

         write(filename,"('kgec-epsilon1-tensor-inter.dat')" )
         open(11,file=filename, action='write')
         write(11,'(A)') &
         '# Dielectric conductivity tensor in the xyz system of reference'
         write(11,'(A)'             ) &
         '#      hbar*w (eV)                           epsilon1(w) (dimensionless)'
         write(11,'(A)',advance='no') &
         '#                               11                  12                  13'
         write(11,'(A)',advance='no') &
         '                  21                  22                  23                  31'
         write(11,'(A)') '                  32                  33               trace/3'
         do iw = iaux, nw
            write(11,'(1p,11es20.7e3)') wgrid(iw), &
                                       (((epsilon1_inter(ix1,ix2,iw)), ix2=1,3),ix1=1,3), &
                                       epsilon1_atrace_inter(iw)
         enddo
         close(11)

         end select ! ac
         
      end select ! decomposition

   
   case( 'atrace' )


      select case( ac )
      case(.true.)

      if ( sigma2_notintra ) then
         write(filename,"('kgec-epsilon1-atrace-inter-only.dat')" )
      else
         write(filename,"('kgec-epsilon1-atrace.dat')" )
      endif

      open(11,file=filename, action='write')
      write(11,'(A)') &
      '# Imaginary part of the average trace of the dielectric &
      &tensor in the xyz system of reference'
      write(11,'(A)'             ) &
      '#      hbar*w (eV)                           tr(epsilon1(w))/3 (dimensionless)'
      write(11,* ) '#'

      do iw = iaux, nw
         write(11,'(1p,2es20.7e3)') wgrid(iw), epsilon1_atrace(iw)
      enddo
      close(11)

      end select ! ac

      select case(dc)
      case(.true.)

         
         if (sigma2_notintra ) then
            write(stdout,*) "      DC ave. trace (inter-band only)          :", epsilon1_dc_atrace
         else
            write(stdout,*) "      DC ave. trace (all contributions)        :", epsilon1_dc_atrace
         endif

      end select

      select case ( decomposition )
      case( .true. )

         ! sigma2
         select case( ac )
         case(.true.)
         
         write(filename,"('kgec-epsilon1-atrace-decomp.dat')" )
         open(11,file=filename, action='write')
         write(11,'(A)') &
         '# Imaginary part of the average of the trace of the dielectric &
         &tensor in the xyz system of reference'

         write(11,'(A)'             ) &
         '#      hbar*w (eV)     tr(epsilon1_intra)/3    tr(epsilon1_degen)/3    &
                                &tr(epsilon1_inter)/3    tr(epsilon1)/3 (dimensionless)'
         write(11,'(A)'             ) '#'
 
         do iw = 0, nw
            write(11,'(1p,6es20.7e3)') wgrid(iw), epsilon1_atrace_intra(iw),   &
                  epsilon1_atrace_degen(iw), epsilon1_atrace_inter(iw), epsilon1_atrace(iw)
         enddo
 
         close(11)


         end select ! ac

         select case( dc )
         case(.true.)

            write(stdout,*) "      DC ave. trace (intra-bands contribution) :", epsilon1_dc_atrace_intra
            write(stdout,*) "      DC ave. trace (degeneracies contribution):", epsilon1_dc_atrace_degen
            write(stdout,*) "      DC ave. trace (inter-bands contribution) :", epsilon1_dc_atrace_inter

         end select

      end select

   end select ! Calculation

   select case( ac )
   case(.true.)

      write(stdout,*) ""
      Write(stdout,*) "    Real part of the dielectric tensor and/or average trace "
      Write(stdout,*) "    vs frequency (in eV) saved to kgec-epsilon1-*.dat file(s)."

   end select ! ac

   end select WRITEEPSILON1

   ! END OF EPSILON1 related output

   !
   ! EPSILON2 OUTPUT
   !

   WRITEEPSILON2: select case( calculate_epsilon2 )
   case( .true. )

   write(stdout,*) ' '
   write(stdout,*)           '    ----------------------------------------------------------------'
   write(stdout,*)           "                      EPSILON2 RELATED OUTPUT "
   write(stdout,*)           '    ----------------------------------------------------------------'

   select case ( dc )
   case( .true. )
        write(stdout,*) '    All epsilon2 DC tensor components are infinity.'
   end select

   select case ( calculation )
   case( 'tensor' )

      ! epsilon2

      select case( ac )
      case(.true.)

      write(filename,"('kgec-epsilon2-tensor-', A2 ,'.dat')" ) deltarep
      open( 11,file=filename, action='write')
      write(11,'(A)')    '# Dielectric conductivity tensor in the xyz system of reference'
      write(11,'(A,L4,2x,A,2x,A2)') '#   sigma1_exact=', sigma1_exact, 'deltarep=', deltarep      
      write(11,'(A,f10.6,A)'     ) '#      deltawidth=', deltawidth, 'eV'
      write(11,'(A)'             ) '#      hbar*w (eV)                           epsilon2(w) ( dimensionless) )'
      write(11,'(A)',advance='no') '#                               11                  12                  13'
      write(11,'(A)',advance='no') '                  21                  22                  23                  31'
      write(11,'(A)') '                  32                  33               trace/3'

      do iw = iaux, nw
         write(11,'(1p,11es20.7e3)') wgrid(iw), (((epsilon2(ix1,ix2,iw)), ix2=1,3),ix1=1,3), &
                                        epsilon2_atrace(iw)
      enddo
      close(11)

      end select ! ac

      select case ( dc )
      case( .true. )

            write(stdout,*) "    epsilon2 DC tensor is infinity for all components."

      end select

         
      select case ( decomposition )
      case(.true.)

         select case( ac )
         case(.true.)
         
         if( .not. sigma1_notintra ) then 

         write(filename,"('kgec-epsilon2-tensor-intra-', A2 ,'.dat')" ) deltarep
         open(11,file=filename, action='write')
         write(11,'(A)') '# Dielectric tensor in the xyz system of reference'
         write(11,'(A,f10.6,A)'     ) '#      deltawidth=', deltawidth, 'eV'
         write(11,'(A)'             ) '#      hbar*w (eV)                           epsilon2(w) (dimensionless)'
         write(11,'(A)',advance='no') '#                               11                  12                  13'
         write(11,'(A)',advance='no') '                  21                  22                  23                  31'
         write(11,'(A)') '                  32                  33               trace/3'
         do iw = iaux, nw
            write(11,'(1p,11es20.7e3)') wgrid(iw), (((epsilon2_intra(ix1,ix2,iw)), ix2=1,3),ix1=1,3), &
                                        epsilon2_atrace_intra(iw)
         enddo
         close(11)
         
         endif
         
         write(filename,"('kgec-epsilon2-tensor-degen-', A2 ,'.dat')" ) deltarep
         open(11,file=filename, action='write')
         write(11,'(A)') '# Dielectric tensor in the xyz system of reference'
         write(11,'(A,f10.6,A)'     ) '#      deltawidth=', deltawidth, 'eV'
         write(11,'(A)'             ) '#      hbar*w (eV)                           epsilon2(w) ( dielectric )'
         write(11,'(A)',advance='no') '#                               11                  12                  13'
         write(11,'(A)',advance='no') '                  21                  22                  23                  31'
         write(11,'(A)') '                  32                  33               trace/3'
         do iw = iaux, nw
            write(11,'(1p,11es20.7e3)') wgrid(iw), (((epsilon2_degen(ix1,ix2,iw)), ix2=1,3),ix1=1,3), &
                                                    epsilon2_atrace_degen(iw)
         enddo
         close(11)


         write(filename,"('kgec-epsilon2-tensor-inter-', A2 ,'.dat')" ) deltarep
         open(11,file=filename, action='write')
         write(11,'(A)') '# Electric conductivity tensor in the xyz system of reference'
         write(11,'(A,f10.6,A)'     ) '#      deltawidth=', deltawidth, 'eV'
         write(11,'(A)'             ) '#      hbar*w (eV)                           epsilon2(w) ( dimensionless )'
         write(11,'(A)',advance='no') '#                               11                  12                  13'
         write(11,'(A)',advance='no') '                  21                  22                  23                  31'
         write(11,'(A)') '                  32                  33               trace/3'
         do iw = iaux, nw
            write(11,'(1p,11es20.7e3)') wgrid(iw), (((epsilon2_inter(ix1,ix2,iw)), ix2=1,3),ix1=1,3), &
                                                   epsilon2_atrace_inter(iw)
         enddo
         close(11)

         end select ! ac

        if ( dc ) write(stdout,*) "       trace of epsilon2 DC tensor is infinity"

         
      end select ! Decomposition

   
   case( 'atrace' )

!      write(stdout,*) "      DDC ave. trace (all contributions)        :", epsilon2_dc_atrace

      select case( ac )
      case(.true.)

      write(filename,"('kgec-epsilon2-atrace-', A2 ,'.dat')" ) deltarep
      open(11,file=filename, action='write')
      write(11,'(A)') '# Dielectric tensor in the xyz system of reference'
      write(11,'(A,L4,2x,A,2x,A2)') '#   sigma1_exact=', sigma1_exact, 'deltarep=', deltarep      
      write(11,'(A)'              ) '#      hbar*w (eV)                           tr(epsilon2(w))/3 ( dimensionless)'
      write(11,'(A)'              ) '#'

      do iw = iaux, nw
         write(11,'(1p,2es20.7e3)') wgrid(iw), epsilon2_atrace(iw)
      enddo
      close(11)

      end select

      select case ( decomposition )
      case( .true. )

         select case( ac )
         case(.true.)
         
         write(filename,"('kgec-epsilon2-atrace-decomp-', A2 ,'.dat')" ) deltarep
         open(11,file=filename, action='write')
         write(11,'(A)') &
         '# Dielectric tensor in the xyz system of reference'
         write(11,'(A,f10.6,A)'     ) '#      deltawidth=', deltawidth, 'eV'

         if ( .not.  sigma1_notintra ) then
         
         write(11,'(A)'             ) &
         '#      hbar*w (eV)     tr(epsilon2_intra)/3    tr(epsilon2_degen)/3    &
                                &tr(epsilon2_inter)/3    tr(epsilon2)/3 (dimensionless)'
         write(11,'(A)'             ) '#'
 
         do iw = iaux, nw
            write(11,'(1p,6es20.7e3)') wgrid(iw), epsilon2_atrace_intra(iw),   &
                  epsilon2_atrace_degen(iw), epsilon2_atrace_inter(iw), epsilon2_atrace(iw)
         enddo
         close(11)

         else
         
         write(11,'(A)'             ) &
         '#      hbar*w (eV)     tr(epsilon2_degen)/3    tr(epsilon2_inter)/3    &
                                               &tr(epsilon2)/3 (dimensionless)'
         write(11,'(A)'             ) '#'
 
         do iw = iaux, nw
            write(11,'(1p,5es20.7e3)') wgrid(iw), epsilon2_atrace_degen(iw), &
                                  epsilon2_atrace_inter(iw), epsilon2_atrace(iw)
         enddo
         close(11)
         
         endif

         end select ! ac

!         select case( dc )
!         case(.true.)

!         if ( .not. notintra ) &         
!         write(*,*) "      DDC ave. trace (intra-bands contribution) :", epsilon2_dc_atrace_intra

!         write(*,*) "      DDC ave. trace (degeneracies contribution):", epsilon2_dc_atrace_degen
!         write(*,*) "      DDC ave. trace (inter-bands contribution) :", epsilon2_dc_atrace_inter

!         end select

      end select

   select case( ac )
   case(.true.)

   write(stdout,*) ""
   Write(stdout,*) "    Imaginary part of the dielectric tensor and/or average trace "
   Write(stdout,*) "    vs frequencies (in eV) saved to kgec-epsilon2-*.dat file(s)."

   end select ! ac

   end select ! ccalculate_epsilon2

   end select WRITEEPSILON2
   !  END OF EPSILON2 OUTPUT 

   ! Restore Frequency grid to Ha
   wgrid = wgrid / AUTOEV

   
   end subroutine kgec_output_epsilon

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   subroutine kgec_output_rrae

   ! Frequency grid in eV
   wgrid = wgrid * AUTOEV


   write(stdout,*) ' '
   write(stdout,*) '    ----------------------------------------------------------------'
   write(stdout,*) '       REFRACTION, REFLECTION, ABSORPTION AND EELS RELATED OUTPUT   '
   write(stdout,*) '    ----------------------------------------------------------------'


      select case ( dc )
      case( .true. )
       
         write(stdout,*) "    DC n_real is infinity."
         write(stdout,*) "    DC n_imag is infinity."
         write(stdout,*) "    DC reflection is one."
         write(stdout,*) "    DC absorption is zero."
         write(stdout,*) "    DC eels is zero."

      end select

      select case ( ac )
      case ( .true. )
      write(filename,"('kgec-rrae-', A2 ,'.dat')" ) deltarep
      open(11,file=filename, action='write')
      write(11,'(A)') &
      '# Refraction, Reflection, Absorption coefficients'
      write(11,'(A)'             ) &
      '#      hbar*w (eV)     n_real    n_imag    reflection    absorption (1/m)    eels'
         write(11,'(A)'             ) '#'
 
         do iw = iaux, nw
            write(11,'(1p,6es20.7e3)') wgrid(iw), n_real(iw), n_imag(iw),   &
                                       reflection(iw), &
                                       absorption(iw)*HARTREE_SI/(H_PLANCK_SI/TPI)*C_AU/C_SI, &
                                       eels(iw) 
         enddo
         close(11)

      end select

   write(stdout,*) ""
   Write(stdout,*) "    Refracction index ( real and imaginary parts), reflection coef.   "
   Write(stdout,*) "    absorption coef. and eels vs frequencies (in eV) saved to kgec-rrae.dat."


   ! Restore Frequency grid to Ha
   wgrid = wgrid / AUTOEV

   end subroutine kgec_output_rrae
  

end module kgecout
