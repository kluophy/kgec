!
! Copyright (C) 2017- Orbital-free DFT group at University of Florida
!
! This program is free software; you can redistribute it and/or modify 
! it under the terms of the GNU General Public License as published by 
! the Free Software Foundation; either version 2 of the License, or 
! any later version. See the file LICENSE in the root directory of the 
! present distribution, or http://www.gnu.org/copyleft/gpl.txt ,
! or contact developers via e-mail: calderin@lcalderin.net, or paper mail:
!
! Quantum Theory Project
! University of Florida
! P.O. Box 118435
! Gainesville, FL 32611
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! Authors: Lazaro Calderin and Valentin Karasiev
!-----------------------------------------------------------------------
! 


module qevars
! Quantum Espresso variables

    use kinds,      ONLY : DP
#if defined(__QE6) || defined(__QE62) || defined(__QE64)
    use io_files,   ONLY : nd_nmbr, prefix, tmp_dir, nwordwfc, iunwfc
#elif defined(__QE54) || defined(__QE512)
    use io_files,   ONLY : nd_nmbr, prefix, outdir, tmp_dir, nwordwfc, iunwfc
#endif
    use io_global,  ONLY : ionode, ionode_id, stdout
#if defined(__QE64)
    use mp_global, ONLY: mp_startup, mp_global_end
    use mp_world,  ONLY: world_comm !, nproc
#else
    use mp_global,  ONLY : mp_startup, mp_global_end, world_comm
#endif
    ! for parallelization over k-points, bands and PW 
    use mp_pools,   ONLY : inter_pool_comm, intra_pool_comm, my_pool_id, me_pool
    use mp_bands,   ONLY : inter_bgrp_comm, intra_bgrp_comm, nproc_bgrp
    use mp,         ONLY : mp_sum, mp_bcast, mp_size, mp_barrier, mp_rank,     &
                           mp_comm_split, mp_comm_group, mp_comm_free,         &
                           mp_group_free
    use constants,  ONLY : PI, TPI, RYTOEV, tpi, ELECTRON_SI, H_PLANCK_SI,     &
                           AUTOEV, HARTREE_SI, BOHR_RADIUS_SI, ELECTRONMASS_SI,&
                           ELECTRONVOLT_SI, SQRTPI, eps6, eV_to_kelvin,        &
                           C_SI, C_AU

#if defined(__QE64)
    use control_flags, ONLY: gamma_only
    use input_parameters, ONLY: conv_thr, wf_collect
#else
    use control_flags, ONLY : twfcollect, gamma_only
    !use input_parameters, ONLY : pseudo_dir, wfcdir, wf_collect
    use input_parameters, ONLY : conv_thr
#endif

    use noncollin_module, ONLY : noncolin, npol
    use environment, ONLY : environment_start, environment_end

    ! Real space unit cell
    use cell_base,  ONLY : tpiba, tpiba2, omega

    ! Reciprocal Space
#if defined(__QE6) || defined(__QE62) || defined(__QE64)
    use klist,      ONLY : nks, xk, wk, ngk, igk_k, nelec, ngauss, degauss, nkstot
#elif defined (__QE54) || defined(__QE512)
    use klist,      ONLY : nks, xk, wk, nelec, ngauss, degauss, nkstot
#endif
    use gvect,      ONLY : ngm, g

    ! Point group symmetry and inversion control variables
    use symm_base,     ONLY : nsym
#if defined(__QE62)
    use input_parameters, ONLY : noinv
#else
    use control_flags, ONLY : noinv
#endif

    ! System Wave Functions
#if defined(__QE6) || defined(__QE62) || defined(__QE64)
    use wvfct,      ONLY : npw, npwx, nbnd, g2kin, wg, et
#elif defined(__QE54)
    use wvfct,      ONLY : npw, npwx, nbnd, igk, g2kin, wg, et
#elif defined(__QE512)
    use wvfct,      ONLY : npw, npwx, nbnd, igk, g2kin, wg, et, ecutwfc
#endif
#if defined(__QE6) || defined(__QE62) || defined(__QE54) || defined(__QE64)
    use gvecw,      ONLY : ecutwfc
#endif
    use lsda_mod,   ONLY : lsda, nspin
#if defined(__QE64)
    use wavefunctions,        ONLY : evc
#else
    use wavefunctions_module, ONLY : evc
#endif

    ! Atoms and pseudo data
    !USE ions_base,      ONLY : nat, ityp, tau
    use uspp,           ONLY : okvan
    use paw_variables,  ONLY : okpaw, only_paw
    !USE atom,           ONLY : msh, rgrid
    !USE paw_onecenter,  ONLY : PAW_potential
    !USE paw_symmetry,   ONLY : PAW_symmetrize_ddd
    !USE uspp_param,     ONLY : upf, nh, nhm, nbetam

    use ener, ONLY : e_f => ef
    implicit none
end module qevars


