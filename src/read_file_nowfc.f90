!
! Copyright (C) 2001-2012 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
! LC: qe read_file sub modified not to read the wavefunctions

SUBROUTINE read_file_nowfc
  !----------------------------------------------------------------------------
  !
  ! Wrapper routine, for compatibility
  !
  USE io_files,             ONLY : nwordwfc, iunwfc, prefix, tmp_dir, wfc_dir
  USE io_global,            ONLY : stdout, ionode
!  USE buffers,              ONLY : open_buffer, close_buffer
  USE wvfct,                ONLY : nbnd, npwx
  USE noncollin_module,     ONLY : npol

#if defined(__QE512) || defined(__QE54)
  USE klist,                ONLY : nks
#endif

  USE paw_variables,        ONLY : okpaw, ddd_PAW
  USE paw_onecenter,        ONLY : paw_potential
  USE uspp,                 ONLY : becsum
  USE scf,                  ONLY : rho
  USE realus,               ONLY : betapointlist, &
                                   init_realspace_vars,real_space
  USE dfunct,               ONLY : newd
  USE ldaU,                 ONLY : lda_plus_u, U_projection

#if defined(__QE6) || defined(__QE54) || defined(__QE512)
  USE pw_restart,           ONLY : pw_readfile
#endif

#if defined(__QE62) || defined(__QE64)
  USE pw_restart_new,       ONLY : read_collected_to_evc
#endif
  USE control_flags,        ONLY : io_level

#if defined(__QE6) || defined(__QE62) || defined(__QE64)
  USE klist,                ONLY : init_igk
  USE gvect,                ONLY : ngm, g
  USE gvecw,                ONLY : gcutw
#endif
 
 !
  IMPLICIT NONE 
  INTEGER :: ierr
  LOGICAL :: exst

#if defined(__QE6) || defined(__QE62) || defined(__QE64)
  CHARACTER( 256 )  :: dirname
  !
  !
  ierr = 0 
#endif

  !
  ! ... Read the contents of the xml data file
  !
#if defined(__QE62) || defined(__QE64)
  dirname = TRIM( tmp_dir ) // TRIM( prefix ) // '.save/'
#endif
  IF ( ionode ) WRITE( stdout, '(/,5x,A,/,5x,A)') &
     'Reading data from directory:', TRIM( tmp_dir ) // TRIM( prefix ) // '.save'
  !
  CALL read_xml_file ( )
  !
  ! ... Open unit iunwfc, for Kohn-Sham orbitals - we assume that wfcs
  ! ... have been written to tmp_dir, not to a different directory!
  ! ... io_level = 1 so that a real file is opened
  !
!  wfc_dir = tmp_dir
!  nwordwfc = nbnd*npwx*npol
!  io_level = 1
!  CALL open_buffer ( iunwfc, 'wfc', nwordwfc, io_level, exst )
  !
  ! ... Allocate and compute k+G indices and number of plane waves
  ! ... FIXME: should be read from file, not re-computed

#if defined(__QE6) || defined(__QE62) || defined(__QE64)
  !
  CALL init_igk ( npwx, ngm, g, gcutw ) 
  !
#endif

#if defined(__QE6) || defined(__QE54) || defined(__512)
  CALL pw_readfile( 'nowave', ierr )
#endif

  !
  ! ... Assorted initialization: pseudopotentials, PAW
  ! ... Not sure which ones (if any) should be done here
  !
  CALL init_us_1()
  !
  IF (lda_plus_u .AND. (U_projection == 'pseudo')) CALL init_q_aeps()
  !
  IF (okpaw) THEN
     becsum = rho%bec
     CALL PAW_potential(rho%bec, ddd_PAW)
  ENDIF 
  !
  IF ( real_space ) THEN
    CALL betapointlist()
    CALL init_realspace_vars()
    IF( ionode ) WRITE(stdout,'(5x,"Real space initialisation completed")')
  ENDIF
!  CALL newd()
  !
!  CALL close_buffer  ( iunwfc, 'KEEP' )
  !
END SUBROUTINE read_file_nowfc

