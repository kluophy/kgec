#
# Makefile for kgec (Kubo-Greenwood Electric Conductivity)
# Prerequisite: QE installed either for parallel or serial execution
# (For QE  versions 5.1.2 5.2.1 5.4.0 6.0 6.1 6.2.1 6.3 6.4.1)
#
# Lazaro Calderin, University of Arizona
# Apr 2019

#
# Edit to choose QE version
#

#QEV=6.4.1
#DQEV=QE64

#QEV=6.3
#DQEV=QE62

#QEV=6.2.1
#DQEV=QE62

#QEV=6.1
#DQEV=QE6

QEV=6.0
DQEV=QE6

#QEV=5.4.0
#DQEV=QE54

#QEV=5.1.2
#DQEV=QE512

#QEV=5.2.1
#DQEV=512

#
# Edit the QE root directory as needed
#

TYPE=mpi
#QE=$(HOME)/programs/qe/$(QEV)-$(TYPE)
#QE=/public/software/apps/qe/intelmpi/6.4.1
QE=/public/home/kluo/work/pkg/qe-$(QEV)


#===================================================================
#
# Load make.inc and redefine/modify QE compilation variables
#

#ifeq ($(strip $(DQEV)),QE64)
#  include $(QE)/make.inc
#  TOPDIR  = $(QE)
#  QEOBJS  = $(QE)/Modules/libqemod.a $(QE)/FFTXlib/libqefft.a 
#  QEOBJS += $(QE)/KS_Solvers/CG/libcg.a $(QE)/KS_Solvers/Davidson/libdavid.a $(QE)/LAXlib/libqela.a 
#  LIBOBJS = $(QEOBJS) $(QE)/iotk/src/libiotk.a $(QE)/UtilXlib/libutil.a $(QE)/clib/clib.a 
#  PPOBJS  = $(QE)/PP/src/libpp.a
#  PWOBJS  = $(QE)/PW/src/libpw.a
#  LIBS    = $(SCALAPACK_LIBS) $(LAPACK_LIBS) $(BLAS_LIBS) $(MPI_LIBS) $(MASS_LIBS) $(HDF5_LIB) $(LIBXC_LIBS) $(LD_LIBS)
#  LIBS   += $(FOX_LIB)
#endif

ifneq (,$(filter $(DQEV), QE62 QE64))
#ifeq ($(strip $(DQEV)),QE62)
  include $(QE)/make.inc
  QEOBJS  = $(QE)/Modules/libqemod.a $(QE)/FFTXlib/libqefft.a 
  QEOBJS += $(QE)/KS_Solvers/CG/libcg.a $(QE)/KS_Solvers/Davidson/libdavid.a $(QE)/LAXlib/libqela.a
  LIBOBJS = $(QEOBJS) $(QE)/iotk/src/libiotk.a $(QE)/UtilXlib/libutil.a $(QE)/clib/clib.a 
  PPOBJS  = $(QE)/PP/src/libpp.a
  PWOBJS  = $(QE)/PW/src/libpw.a
endif

ifeq ($(strip $(DQEV)),QE6)
  include $(QE)/make.inc
  QEOBJS  = $(QE)/Modules/libqemod.a $(QE)/FFTXlib/libqefft.a $(QE)/LAXlib/libqela.a
  LIBOBJS = $(QEOBJS) $(QE)/clib/clib.a $(QE)/iotk/src/libiotk.a
  PPOBJS  = $(QE)/PP/src/openfil_pp.o
  PWOBJS  = $(QE)/PW/src/libpw.a
endif

ifneq (,$(findstring QE54, $(DQEV)))
  include $(QE)/make.sys
  QEOBJS  = $(QE)/Modules/libqemod.a $(QE)/FFTXlib/libqefft.a $(QE)/LAXlib/libqela.a
  LIBOBJS = $(QEOBJS) $(QE)/clib/clib.a $(QE)/iotk/src/libiotk.a
  PPOBJS  = $(QE)/PP/src/openfil_pp.o
  PWOBJS  = $(QE)/PW/src/libpw.a
endif

ifneq (,$(findstring QE51, $(DQEV)))
  include $(QE)/make.sys
  QEOBJS  = $(QE)/Modules/libqemod.a
  LIBOBJS = $(QE)/flib/ptools.a $(QE)/flib/flib.a $(QE)/clib/clib.a $(QE)/iotk/src/libiotk.a
  PPOBJS  = $(QE)/PP/src/openfil_pp.o
  PWOBJS  = $(QE)/PW/src/libpw.a
endif

ifneq (,$(findstring QE52, $(DQEV)))
  include $(QE)/make.sys
  QEOBJS  = $(QE)/Modules/libqemod.a
  LIBOBJS = $(QE)/flib/ptools.a $(QE)/flib/flib.a $(QE)/clib/clib.a $(QE)/iotk/src/libiotk.a
  PPOBJS  = $(QE)/PP/src/openfil_pp.o
  PWOBJS  = $(QE)/PW/src/libpw.a
endif


F90FLAGS += -D__$(DQEV)

ifneq (,$(findstring GFORTRAN, $(DFLAGS)))
 MPIF90 += -ffree-line-length-none
 F77    += -ffree-line-length-none
endif

#MPIF90 += -ffree-line-length-none
#F77    += -ffree-line-length-none

MODFLAGS = -I $(QE)/Modules -I $(QE)/PW/src -I ./

#ifeq ($(strip $(DQEV)),QE64)
# MODFLAGS += -I $(QE)/UtilXlib -I $(QE)/LAXlib
#endif

ifneq (,$(filter $(DQEV), QE62 QE64 ))
#ifeq ($(strip $(DQEV)),QE62)
 MODFLAGS += -I $(QE)/UtilXlib -I $(QE)/LAXlib
endif


#F90FLAGS += -O0 -debug all  -check all  -traceback  -fpp -D__$(DQEV)
#-fpe3 #-ftrapuv #-fpe:0

KGOBJS = \
./qevars.o \
./apawgm.o \
./qe_p_psi.o \
./kgecvars.o \
./kgsigma1.o \
./kgsigma2.o \
./parallel.o \
./kgecchecks.o \
./kgecpaw.o \
./kgecgradient.o \
./kgecout.o \
./kgsigma2_w.o \
./kgepsilon1.o \
./kgepsilon2.o \
./kgrrae.o \
./read_file_nowfc.o

export 

SRCDIR=./for-qe-$(QEV)-$(TYPE)

#===================================================================

all :
	(cp -r ./src $(SRCDIR); cp makefiles/Makefile.for-qe-$(QEV) $(SRCDIR); \
         cd $(SRCDIR); pwd; $(MAKE) -f Makefile.for-qe-$(QEV) all || exit 1)

clean :
	rm -fr $(SRCDIR)

