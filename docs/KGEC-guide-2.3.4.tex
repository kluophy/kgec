\documentclass[apos,article,onecolumn,12pt,prb] {revtex4-2}
%\documentclass[aps,onecolumn,12pt,prb] {revtex4-1}

%% Updates for KEGEC 2.3.2, use KGEC version as the guide version, LC
%% ``Guide'' v1d 20 June 2017 SBT, small updates 
%% ``Guide'' v1c 19 June 2017 SBT, from v1b LC same date
%% ``Guide'' Version 1a, 19 June 2017 SBT, small edits, added Profess@QE
%%% ``Guide'' Version 1, Jun 17 2017, LC update to 
%%  include latest implementations,
%%% answer SBT questions and 
%%% Name changed to KGEC-guide, LC
%%% KGEC README vers 1a, 13vi17, SBT -small edits and inserted
%% questions
%%% KGEC README version 1, 08v17, SBT
\usepackage{epsf,amsmath,amsfonts}

\usepackage{graphics,psfrag}
\usepackage{graphicx,psfrag}

\usepackage{wrapfig}
\usepackage{color}
\usepackage{hyperref}
\usepackage[toc,page]{appendix}
%\usepackage[inline]{showlabels}
\usepackage{titlesec}
\usepackage{verbatim}

%\bibliographystyle{unsrt} 
%\usepackage{natbib}

\usepackage{xcolor}
\hypersetup{
    colorlinks,
    linkcolor={blue!80!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}

%\pagestyle{plain}
%\setlength{\paperheight}{11in}
%\setlength{\paperwidth}{8.5in}
%\setlength{\footskip}{1.0in}
%\setlength{\textwidth}{6.5in}
%\setlength{\textheight}{9.0in}

\setcounter{secnumdepth}{4}

%\pagestyle{plain}
%\topmargin -1.4cm
%\oddsidemargin -0.cm 
%\evensidemargin -0.cm
%\textheight 8.9in 
%\textwidth 6.4in 
%\sloppy
%\def\newline{\hfil\break} 

\newcommand{\BIG}[2]{\mbox{$\left#1\vbox to #2{}\right. $}}
\newcommand{\bea}{\begin{eqnarray}}
\newcommand{\eea}{\end{eqnarray}}
\newcommand{\beast}{\begin{eqnarray*}}
\newcommand{\eeast}{\end{eqnarray*}}
\newcommand{\be}{\[}
\newcommand{\ee}{\]}
\newcommand{\beq}{\begin{equation}}
\newcommand{\eeq}{\end{equation}}
\newcommand{\half}{\frac{1}{2}}
\renewcommand{\vr}{(\mathbf{r})}
\newcommand{\intdr}{{\int}d^3r\,}
\newcommand{\figref}[1]{Fig.~\ref{#1}}
\newcommand{\figtworef}[2]{Figures~\ref{#1} and \ref{#2}}
\newcommand{\tabref}[1]{Table~\ref{#1}}
\newcommand{\tabtworef}[2]{Tables~\ref{#1} and \ref{#2}}
\newcommand{\secref}[1]{Sec.~(\ref{#1})}
\newcommand{\appref}[1]{App.~(\ref{#1})}
\newcommand{\eqnref}[1]{Eq.~(\ref{#1})}
\newcommand{\eqntworef}[2]{Eqs.~(\ref{#1}) and (\ref{#2})}
\newcommand{\chpref}[1]{Chapter~\ref{#1}}
\newcommand{\bra}[1]{ \langle {#1} |}
\newcommand{\ket}[1]{| {#1} \rangle }
\newcommand{\spr}[2]{ \langle {#1} | {#2} \rangle }
\renewcommand{\v}[1]{\mathbf{#1}}
\newcommand{\refsec}[1]{ Sec.~\ref{#1} }
\newcommand{\refapp}[1]{ App.~\ref{#1} }
%\newcommand{\paper}[2]{Ref. \cite{#1.#2} }
\newcommand{\ltp}{\left(}
\newcommand{\rtp}{\right)}

\begin{document}
 
 \vspace*{-0.8cm}\noindent
\begin{center}
{\large\textbf {KGEC USER GUIDE}} \\
{\large\textbf {Kubo-Greenwood Electrical Conductivity Code}} \\  
{\small L.~Calder\'in \\ University of Arizona\\ V. Karasiev \\ Rochester University \\ S.B.\ Trickey \\ Univ.\ Florida} \\
\vspace*{-4pt}{\small 9 Oct 2019; version 2.3.4}
\\ \end{center}

\renewcommand{\baselinestretch}{0.98}
\large\normalsize 

\section{Basics}
\vspace*{-20pt}
 \noindent 
\begin{list}{-}
\item KGEC (Kubo-Greenwood Electrical Conductivity) is a post-processor module 
for use with Quantum Espresso. QE versions 5.1.2, 5.2.1, 5.4.0, 6.0, 6.1 and 
6.2.1, 6.3 and 6.4.1 are currently supported. 

KGEC calculates the complex conductivity and dielectric tensors as well as
the refraction, reflection, absorption coefficients and electron energy loss
spectra. 


QE 5.2.1 compiled for use in the Profess@QE suite
[Comput.\ Phys.\ Commun.{\ \textbf{185}}, 3240 (2014)] also is supported. 
KGEC runs at both zero and non-zero temperature, with the temperature passed 
to  KGEC via the occupation numbers (and associated Kohn-Sham orbitals  
and eigenvalues) from the QE calculation. Note that Profess@QE enables 
use of free-energy exchange-correlation functionals [e.g.\ Phys.\ Rev.\ Lett.\ 
\textbf{112}, 076403 (2014); Phys.\ Rev.\ E \textbf{93}, 063207 (2016); arXiv 1612.06266v2].  
It also enables use of finite-temperature
orbital-free DFT [Phys.\ Rev.\ B \textbf{88}, 161108(R) (2013)] for 
thermodynamic properties, including snapshot averages of electrical conductivity.  
\item
\item Primary code designer: L{\'a}zaro Calder{\'i}n (calderin@lcalderin.net). 

\item Other code authors and testers, Valentin Karasiev (vkarasev@qtp.ufl.edu), 
Sam Trickey (trickey@qtp.ufl.edu).  \vspace*{-3pt} 

\item Licensure: GNU GPL.   \vspace*{-3pt} 

\item Main reference: ``Kubo-Greenwood Electrical Conductivity Formulation 
and Implementation for Projector Augmented Wave Datasets'', L.\ Calder\'in, 
V.V.\ Karasiev, and S.B.\ Trickey (in preparation for submittal to 
Comput.\ Phys.\ Commun.\ as of this date). 
\end{list}
 \vspace*{-12pt} 

\section{Definitions, Abbreviations, and Notation}
\vspace*{-20pt}
 \noindent 
\begin{list}{}{}
  \setlength{\itemsep}{1pt}
  \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt}
\item $\omega$: frequency  %\vspace*{-4pt} 
\item $\sigma_1(\omega)$: real part of the complex conductivity $\sigma(\omega)$ tensor  %\vspace*{-4pt} 
\item $\sigma_2(\omega)$:  imaginary part of the complex conductivity $\sigma(\omega)$ tensor  %\vspace*{-4pt} 
\item $\v{k};n,n'$: vector in the Brillouin zone; band indices  %\vspace*{-4pt} 
\item $\Omega; w_\v{k}$: unit cell volume; reciprocal space integration weights %\vspace*{-4pt} 
\item $\Delta \epsilon_{n\v{k},n'\v{k}}$: difference in eigenvalues for 
bands $n$ and $n'$ at fixed $\v{k}$  %\vspace*{-4pt} 
\item $\Delta f_{n\v{k},n'\v{k}}$: difference in Fermi-Dirac occupation numbers 
for bands $n$ and $n'$  at fixed $\v{k}$  %\vspace*{-4pt} 
\item $\delta(x)$: Dirac delta function  %\vspace*{-4pt} 
\item AC: Alternating Current (i.e.\ frequency dependent)  %\vspace*{-4pt} 
\item DC: Direct Current (i.e.\ frequency independent)  %\vspace*{-4pt} 
\item KS: Kohn-Sham  %\vspace*{-3pt} 
\item PAW: Projector Augmented Wave  %\vspace*{-4pt} 
\item QE: Quantum Espresso 
\end{list}
 \vspace*{-20pt}
 
\subsection{Prerequisites and Assumptions}
%
\vspace*{-4pt} 
\noindent Prerequisites for the installation of KGEC are:
\begin{itemize}
 \item Fortran 90 compiler (Makefiles for Intel or GNU Linux Fortran provided).
\vspace*{-4pt} 
 \item QE 5.1.2, 5.2.1, 5.4.0, 6.0 or 6.1 installed for either serial or 
mpi-parallel execution (or QE 5.2.1 modified and installed using Intel for 
use with Profess@QE). \vspace*{-4pt} 
%\item Linux with Bourne-again shell (bash) \vspace*{-4pt} 
\item MPI for parallel compilation.
\end{itemize}
\vspace*{-4pt} 

KGEC operation assumes that an ordinary QE calculation has been done with
QE options set to wf\_collect=.true. , smearing='fd', nosym=.true.,
noinv=.true.  and any of the options for KPOINTS except "KPOINTS gamma".\\


The QE
calculation provides the KS orbitals, orbital energies, occupation numbers, 
and other
relevant data via storage in the usual outdir directory. All of that data
is made accessible to KGEC by the QE\_VAR and QE\_P\_PSI modules.
Note that KGEC uses the \texttt{make.sys} file  generated for QE 5.x during 
QE installation  or equivalently, the  
\texttt{make.inc} file for QE-6.x .\\ \vspace*{-14pt}

\subsection{Installation and Trial Run Example}
\vspace*{-10pt}

It is impossible to provide instructions for all operating systems 
and compilers. Here we provide instructions tested with Linux systems
of the Red Hat family versions 6 and 7 (Red Hat, CentOS, Fedora 20-25)
and SUSE Linux 11 (Cray)  using 
Intel compilers and the Bourne-again shell (bash). These instructions 
should be relatively easy to adapt to other computational environments.\\
\vspace*{-18pt}
\subsubsection{Installation}
%\noindent Installation - 
\begin{itemize}
\item Download the KGEC package from www.qtp.ufl.edu/ofdft . \vspace*{-4pt} 
\item Unzip the package to the kgec root directory named
KGECDIR, and change into that directory. \vspace*{-4pt} 
 \item Modify the appropriate Makefile in KGECDIR to get the 
variable QE to point to 
the path to the root directory of your QE installation. 
\emph{Notice} that the last segment of the path must 
be of the form \$QEV-\$TYPE, where \$QEV is the version of QE 
and \$TYPE is either 'serial' or 'mpi'. For example, use 6.0-mpi for 
QE version 6 compiled with MPI. To comply with this requirement, one 
may create a symbolic link with the required name to the actual 
QE installation. An alternative is to edit the variables QE and 
SRCDIR to fit your needs. Also note that for use with QE 5.2.1 modified 
and compiled 
for incorporation in Profess@QE, use Makefile.for-qe-5.2.1m5 
(tested only with Intel compilers). \vspace*{-4pt} 
\item Run \texttt{make} on this directory to create ./for-qe-\$QEV-\$TYPE/kgec.x
\end{itemize}
\vspace*{-6pt} 

\subsubsection{Trial Run}
\vspace*{-10pt}
Input data and output for a trial example are provided.  It is a nearly trivial case,
namely fcc Aluminum with 4 atoms per unit cell at bulk density $\rho
=2.7 $ g/cm$^3$ and temperature = 0.05 Ry = 0.683 eV= 7894.37 K. 
The PAW dataset 
provided was generated with the ATOMPAW code 
(see Al.UPF file for more details). 
Detailed descriptions of input and output are 
given below. 

To run the trial example, proceed as follows:
\begin{itemize}
 \item Change to the ./example/run directory
 \item Execute QE for the input file provided (Al-fcc-4.in)
 \item Once QE has finished, run kegc.x in the same directory; see 
run command details below. 
 \item Compare the resulting output files with those in ./example/reference 
See detailed description of output below.  Note that if you are using QE compiled for use with Profess@QE, you should compare with 
./example/reference/for-qe-5.2.1m5-serial  
or ./example/reference/for-qe-5.2.1m5-mpi as appropriate.   
\end{itemize}

For serial KGEC execution, use the command:
\begin{verbatim}
 KGECDIR/kegec.x < INPDIR/kegec.in > OUTDIR/kgec.out
\end{verbatim}

For parallel KGEC execution, use the $-n$, $-nk$, $-nb$, and $-np$ parameters 
and the corresponding MPIEXEC command of your parallel environment as
done for QE.  For example, to run 8 processes with 2 each for k-points, 
bands, and plane waves, issue the command:
\begin{verbatim}
mpiexec -n 8 KGEC_DIR/kegec.x -nk 2 -nb 2 -np 2 < INP_DIR/kegec.in 
> OUT_DIR/kgec.out
\end{verbatim}
\vspace*{-6pt} 

\subsection{Input and Output}
\vspace*{-10pt}
\noindent The input file contains a Fortran card image named kgecpp.  All 
the possible variables are described in \tabref{tab:kgec-input}.
An input file example is 
{%\smallsize %\scriptsize
\begin{verbatim}
&kgecpp
  outdir='./',
  prefix='al'
  calculation='tensor'
  sigma1_exact=.true.
  decomposition=.true.
  calculate_sigma2=.true.
/
\end{verbatim}
}

Notice that at minimum the input file must contain the 'outdir' 
and 'prefix' variables.

\begin{table}[h!]
\begin{tabular}{l p{4in}}
   \hline
   \multicolumn{1}{c}{Variable}  & \multicolumn{1}{c}{Description} \\
   \hline
   outdir                        &  Output directory as specified in the QE calculation.  \\
   prefix                        &  The prefix variable as specified for the QE calculation. \\
   ac (=.true.)                  &  Calculate the frequency-dependent conductivity. \\
   dc (=.true.)                  &  Calculate the DC conductivity. \\
   calculation (='atrace')       &  Use 'tensor' for the full tensor, 
                                   'atrace'  for the average trace.  \\
   sigma1\_exact (=.true.)       &  Use exact or approximated formula. \\
   sigma1\_notintra (=.false.)   &  Do not include intra-band for 
                                   the exact formula (only valid with sigma1\_exact=.true.). \\
   calculate\_sigma2 (=.false.)  &  Calculate the imaginary part of the conductivity.\\
   sigma2\_notintra (=.false.)   &  Do not include intra-band and degenerate contributions
                                   (only valid with calculate\_sigma2=.true.). \\
   calculate\_epsilon1 (=.false.) &  Calculate the real part of the dielectric function.\\
   calculate\_epsilon2 (=.false.) &  Calculate the imaginary part of the dielectric function.\\
   calculate\_rrae (=.false.)     &  Calculate the refraction indexes, reflection coef., 
                                     absorption coef. and electron energy loss spectrum.\\

   decomposition (=.true.)      &  Separate contributions into intra-band, degenerate, and
                                   inter-band (only possible with sigma1\_exact=.true., 
                                   sigma1\_notintra=.false. and sigma2\_notintra=.false).\\
   writegm (=.false.)           &  Write gradient matrix elements to disk. \\
   readgm (=.false.)            &  Read gradient matrix elements from disk. \\
   check\_wfc   (=.false.)      &  Check the orthogonality of orbitals. \\
   check\_delta (=.false.)      &  Compare the effect of Lorentzians and
                                   Gaussians as delta function representations.\\
   deltarep (='2l')             &  Representation for the Dirac delta function:
                                   use  'l' or '1l' for a Lorentzian, '2l' for two Lorentzians,
                                   'g' or '1g' for a Gaussian, '2g' for two Gaussians 
                                   (case insensitive).\\
   deltawidth (=0.01)           &  Width of the Dirac delta function representation in eV.\\
   wmin        (=0.01)          &  Minimum frequency in eV. \\
   wmax        (=5.0)           &  Maximum frequency in eV (wmin fixed to zero in the code).  \\
   nw          (=1000)          &  Number of frequency steps, less one, in the closed 
                                   interval $[wmin,wmax]$ \\
   non\_local  (=.false.)       &  Use a norm-conserving pseudo-pot even though
                                   non-local corrections to matrix elements 
                                   are not implemented.\\
   npwrecover (=.false)         &  Recover pw processes for band parallelization. \\
   yessym (=.false.)            &  Over-ride the non-use of symmetry\\
   yesinv   (=.false.)          &  Over-ride the non-use of inversion symmetry\\
    \hline
\end{tabular}
\caption{\label{tab:kgec-input} KGEC input variables. Default values given in parentheses.}
\end{table}

\clearpage

During execution, KGEC writes information both to standard output
and to specific files.  A standard output example 
follows. Notice that it is fully commented and provides information about 
the files written to disk and their content.

%\begin{center}
{\verbatiminput{kgec.out}}
%\end{center}


\section{Acknowledgments}
\noindent KGEC code development was supported by U.S.\ Dept.\ of Energy
grant DE-SC 0002139. 

\end{document}

